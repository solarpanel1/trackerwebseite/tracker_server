const {defaults} = require('jest-config')
module.exports = {
    // ...
    moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
    // ...
    displayName: {
        name: 'CLIENT',
        color: 'blue',
    }
}