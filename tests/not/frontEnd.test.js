const request = require('supertest')
const app = require('../../src/app')

test('Mit await', async () => {
    await request(app)
        .get('/ggg')
        .send()
        .expect(201)
})

describe('GET /ggg', function () {
    it('responds with json', async function () {
        const response = await request(app)
            .get('/ggg')
        expect(response.status).toEqual(201)
    })
})

test('Abschluss', (done) => {
    setTimeout(() => {
        done()
    }, 100)
})