require('dotenv').config()
const MQTT = require("async-mqtt")
import pm2apps from '../../config/ecosystem.config.cjs'

const env = pm2apps.apps[0].env

var options = {
    clientId: env.TTN_CLIENTID,
    port: env.TTN_PORT,
    keepalive: 60,
    username: env.TTN_USERNAME,
    password: env.TTN_TOKEN
}

const client = MQTT.connect(env.TTN_BROKER_URL, options)

const awtMsg = async () => {
    return new Promise((resolve) => {
        client.on('connect', function () {
            client.subscribe('#')
            client.on('message', function (topic, message) {
                resolve(true)
                client.end()
            })
        })
    })
}

jest.setTimeout(500000)
test('MQTT Testing', async () => {
    const response = await awtMsg()
    expect(response).toBe(true)
})