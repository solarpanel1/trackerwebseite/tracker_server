const add = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (a < 0 || b < 0) {
                return reject('Numbers must be non-negative')
            }
            resolve(a + b)
        }, 250)
    })
}

test('Should add two numbers async/await', async () => {
    const sum = await add(10, 20)
    expect(sum).toBe(30)
})

test('Mit Promises', (done) => {
    add(2, 3).then((sum) => {
        expect(sum).toBe(5)
        done()
    })
})

test('Test mit done()', (done) => {
    setTimeout(() => {
        expect(1).toBe(1)
        done()
    }, 250)
})

