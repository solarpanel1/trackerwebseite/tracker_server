// const create_entry = require('../src/utils/mariaDB.js')
const {
    mariadbCreateDB,
    mariadbDropDB,
    mariadbClearDB,
    mariadbCreateEntry,
    mariadbConnect
} = require('../src/utils/mariaDB.js')

// beforeEach(async () => {
//     await clearDB()
// })

beforeAll(async () => {
    // await clearDB()
    await mariadbDropDB()
    await mariadbCreateDB()
})

function myDate() {
    let myDate = new Date().toISOString().replace('T', ' ').substring(0, 19)
    return (myDate)
}

test('mariaDB1', async () => {
    const createdOn = myDate()
    const sensorTemp = '8.1 °C'
    const sensorHumidity = '65%'
    const consumedAirtime = '0.164864'
    const RSSI = '-123'
    const SNR = '6'
    const channelRSSI = '-123'
    const gatewayID = 'gw-miracuru-zrh'
    const gatewayEUI = 'A0F1C2D3A4B5C6D7'
    const deviceID = 'eui-70b3d57ed0047554'
    const deviceAddr = '260BF700'
    const frequency = '868300000'
    const spreadingFactor = '9'
    const message = "{\"Test 1\":\"Test 123\",\"testEntry2\":\"Test 456\"}"
    const response = await mariadbCreateEntry(createdOn, sensorTemp, sensorHumidity, consumedAirtime, RSSI, SNR, channelRSSI, gatewayID, gatewayEUI, deviceID, deviceAddr, frequency, spreadingFactor, message)
    expect(response).toBe('passed')
})

test('mariaDB1', async () => {
    const createdOn = myDate()
    const sensorTemp = '8.1 °C'
    const sensorHumidity = '65%'
    const consumedAirtime = '0.164864'
    const RSSI = '-123'
    const SNR = '6'
    const channelRSSI = '-123'
    const gatewayID = 'gw-miracuru-zrh'
    const gatewayEUI = 'A0F1C2D3A4B5C6D7'
    const deviceID = 'eui-70b3d57ed0047554'
    const deviceAddr = '260BF700'
    const frequency = '868300000'
    const spreadingFactor = '9'
    const message = "{\"Test 1\":\"Test 123\",\"testEntry2\":\"Test 456\"}"
    const response = await mariadbCreateEntry(createdOn, sensorTemp, sensorHumidity, consumedAirtime, RSSI, SNR, channelRSSI, gatewayID, gatewayEUI, deviceID, deviceAddr, frequency, spreadingFactor, message)
    expect(response).toBe('passed')
})

test('mariaDB2', async () => {
    const createdOn = myDate()
    const sensorTemp = '6 °C'
    const sensorHumidity = '60%'
    const consumedAirtime = '0.164864'
    const RSSI = '-123'
    const SNR = '6'
    const channelRSSI = '-123'
    const gatewayID = 'gw-miracuru-zrh'
    const gatewayEUI = 'A0F1C2D3A4B5C6D7'
    const deviceID = 'eui-70b3d57ed0047554'
    const deviceAddr = '260BF700'
    const frequency = '868300000'
    const spreadingFactor = '9'
    const message = "{\"Test 1\":\"Test 123\",\"testEntry2\":\"Test 456\"}"
    const response = await mariadbCreateEntry(createdOn, sensorTemp, sensorHumidity, consumedAirtime, RSSI, SNR, channelRSSI, gatewayID, gatewayEUI, deviceID, deviceAddr, frequency, spreadingFactor, message)
    expect(response).toBe('passed')
})
