# Solartracker ttnServer API

## Setup

1. Create configuration file <code>./config/ecosystem.config.cjs</code> see example file in this folder.
2. Install packets with <code>npm -i</code>
3. install pm2: <code>npm install pm2 -g</code>
4. Start the server API:
   - Production: <code>pm2 start ./config/ecosystem.config.cjs --env production</code>
   - Development: <code>pm2 start ./config/ecosystem.config.cjs</code>
5. Stop application with <code>pm2 stop \<name\></code> or <code>\<number\></code>
6. Delete application with <code>pm2 \<del|delete\></code> or <code> \<number\></code> or <code>\<all\></code>

## Documentation for pm2<br>
https://pm2.keymetrics.io/docs/usage/quick-start/<br>
https://www.npmjs.com/package/pm2