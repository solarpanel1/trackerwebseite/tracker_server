*
attach <pm_id> [comman]                                      attach stdin/stdout to application identified by <pm_id>
autoinstall
backward <name>                                              downgrades repository to the previous commit for a given app
cleardump                                                    Create empty dump file
conf [key] [value]                                           get / set module config values
config <key> [value]                                         get / set module config values
create                                                       return pid of [app_name] or all
dashboard|dash                                               launch dashboard with monitoring and logs
deepUpdate                                                   performs a deep update of PM2
delete|del <name|id|namespace|script|all|json|stdin...>      stop and delete a process from pm2 process list
deploy <file|environment>                                    deploy your json
desc <name|id>                                               (alias) describe all parameters of a process
describe <name|id>                                           describe all parameters of a process
dump|save [options]                                          dump all processes for resurrecting them later
ecosystem|init [mode]                                        generate a process conf file. (mode = null or simple)
env <id>                                                     list all environment variables of a process id
examples                                                     display pm2 usage examples
flush [api]                                                  flush logs
forward <name>                                               updates repository to the next commit for a given app
get [key]                                                    get value for <key>
id <name>                                                    get process id by name
imonit                                                       launch legacy termcaps monitoring
info <name|id>                                               (alias) describe all parameters of a process
inspect <name>                                               inspect a process
install|module:install [options] <module|git:/>              install or update a module and run it forever
jlist                                                        list all processes in JSON format
kill                                                         kill daemon
l                                                            (alias) list all processes
link [options] [secret] [public] [name]                      link with the pm2 monitoring dashboard
list|ls                                                      list all processes
login                                                        Login to pm2 plus
logout                                                       Logout from pm2 plus
logrotate                                                    copy default logrotate configuration
logs [options] [id|name|namespace]                           stream logs file. Default stream all logs
module:generate [app_name]                                   Generate a sample module in current folder
module:update <module|git:/>                                 update a module and run it forever
monit                                                        launch termcaps monitoring
monitor [name]                                               monitor target process
multiset <value>                                             multiset eg "key1 val1 key2 val2
open                                                         open the pm2 monitoring dashboard
package [target]                                             Check & Package TAR type module
pid [app_name]                                               return pid of [app_name] or all
ping                                                         ping pm2 daemon - if not up it will launch it
plus|register [options] [command] [option]                   enable pm2 plus
prettylist                                                   print json in a prettified JSON
profile:cpu [time]                                           Profile PM2 cpu
profile:mem [time]                                           Sample PM2 heap memory
ps                                                           (alias) list all processes
publish|module:publish [options] [folder]                    Publish the module you are currently on
pull <name> [commit_id]                                      updates repository for a given app
reload <id|name|namespace|all>                               reload processes (note that its for app using HTTP/HTTPS)
reloadLogs                                                   reload all logs
report                                                       give a full pm2 report for https://github.com/Unitech/pm2/issues
reset <name|id|all>                                          reset counters for process
restart [options] <id|name|namespace|all|json|stdin...>      restart a process
resurrect                                                    resurrect previously dumped processes
scale <app_name> <number>                                    scale up/down a process in cluster mode depending on total_number param
send <pm_id> <line>                                          send stdin to <pm_id>
sendSignal <signal> <pm2_id|name>                            send a system signal to the target process
serve|expose [options] [path] [port]                         serve a directory over http via port
set [key] [value]                                            sets the specified config <key> <value>
show <name|id>                                               (alias) describe all parameters of a process
slist|sysinfos [options]                                     list system infos in JSON
start [options] [name|namespace|file|ecosystem|id...]        start and daemonize an app
startOrGracefulReload <json>                                 start or gracefully reload JSON file
startOrReload <json>                                         start or gracefully reload JSON file
startOrRestart <json>                                        start or restart JSON file
startup [platform]                                           enable the pm2 startup hook
status                                                       (alias) list all processes
stop [options] <id|name|namespace|all|json|stdin...>         stop a process
sysmonit                                                     start system monitoring daemon
trigger <id|proc_name|namespace|all> <action_name> [params]  trigger process action
uninstall|module:uninstall <module>                          stop and uninstall a module
unlink                                                       unlink with the pm2 monitoring dashboard
unmonitor [name]                                             unmonitor target process
unset <key>                                                  clears the specified config <key>
unstartup [platform]                                         disable the pm2 startup hook
update                                                       (alias) update in-memory PM2 with local PM2
updatePM2                                                    update in-memory PM2 with local PM2
.