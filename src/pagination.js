router.get('/items/:page', (req, res) => {
    const dbx = require('mysql2'),
        Pagination = require('./utils/pagination.js'),

        // Get current page from url (request parameter)
        page_id = parseInt(req.params.page),
        currentPage = page_id > 0 ? page_id : currentPage,

        //Change pageUri to your page url without the 'page' query string 
        pageUri = '/items/'

    /*Get total items*/
    dbx.query('SELECT COUNT(id) as totalCount FROM items', (err, result) => {

        // Display 10 items per page
        const perPage = 10,
            totalCount = result[0].totalCount

        // Instantiate Pagination class
        const Paginate = new Pagination(totalCount, currentPage, pageUri, perPage)


        /*Query items*/
        dbx.query('SELECT * FROM items LIMIT ' + Paginate.perPage + ' OFFSET ' + Paginate.start, (err, result) => {

            data = {
                items: result,
                pages: Paginate.links()
            }

            // Send data to view
            res.render('items', data)
        })
    })

})
