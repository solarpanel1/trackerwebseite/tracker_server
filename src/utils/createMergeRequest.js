import fetch from 'node-fetch'
let url = "https://gitlab.com/api/v4/projects/34946389/merge_requests"

const body = {
    title: "Dev => Main (automatically generated)",
    description: "This merge request was generated from 'createMergeRequest.js'",
    source_branch: "dev",
    target_branch: "main",
    remove_source_branch: "false"
}

const response = await fetch(url, {
    method: 'post',
    body: JSON.stringify(body),
    headers: {
        'Content-Type': 'application/json',
        'PRIVATE-TOKEN': 'gjfsSSz4aBzS-wXEqkmt'
    }
});
const data = await response.json();

console.log(data);