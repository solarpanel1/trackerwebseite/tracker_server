import pm2 from 'pm2'
import { argv } from 'process';

const myEnv = argv[2]
delProcessDEV(myEnv)

function diconnectAfterDelay() {
    return new Promise(resolve => {
        setTimeout(() => {
            pm2.disconnect()
            resolve('Success')
        }, 1250);
    })
}

async function delProcessDEV(myEnv) {
    pm2.connect(function(err) {
        if(err) {
            console.error(err)
            process.exit(2)
        }

        pm2.delete(myEnv, (err) => {
            if(err) { console.error(err); }
            console.log(`Deleted Process: ${myEnv}`);
        })
    })

    const result = await diconnectAfterDelay()
    console.log(`The Result: ${result}`);
}