import mysql from 'mysql2'
import pm2apps from '../../config/ecosystem.config.cjs'

// TODO env Variable funktioniert noch nicht
const env = pm2apps.apps[0].env
const connection = mysql.createConnection({
    host: env.SQL_HOST,
    user: env.SQL_USER,
    password: env.SQL_PASSWORD,
    database: env.SQL_DB
})

export default connection