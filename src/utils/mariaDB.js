import mysql from 'mysql2'
import pm2apps from '../../config/ecosystem.config.cjs'

let SQL_HOST
let SQL_USER
let SQL_PASSWORD
let SQL_DB
let SQL_TABLE

const Umgebung = process.env.NODE_ENV
if(Umgebung === 'prod') {
    SQL_HOST = pm2apps.apps[0].env_production.SQL_HOST
    SQL_USER = pm2apps.apps[0].env_production.SQL_USER
    SQL_PASSWORD = pm2apps.apps[0].env_production.SQL_PASSWORD
    SQL_DB = pm2apps.apps[0].env_production.SQL_DB
    SQL_TABLE = pm2apps.apps[0].env_production.SQL_TABLE
}
else if(Umgebung === 'dev') {
    SQL_HOST = pm2apps.apps[0].env_production.SQL_HOST
    SQL_USER = pm2apps.apps[0].env_production.SQL_USER
    SQL_PASSWORD = pm2apps.apps[0].env_production.SQL_PASSWORD
    SQL_DB = pm2apps.apps[0].env_production.SQL_DB
    SQL_TABLE = pm2apps.apps[0].env_production.SQL_TABLE
}

const xconnection = async () => {
    const connection = mysql.createConnection({
        host: SQL_HOST,
        user: SQL_USER,
        password: SQL_PASSWORD,
        database: SQL_DB
    })
}

const connection = mysql.createConnection({
    host: SQL_HOST,
    user: SQL_USER,
    password: SQL_PASSWORD,
    database: SQL_DB
})

const mariadbConnect = async () => {
    return new Promise((resolve, reject) => {
        connection.connect(function(err) {
            if(err) throw err
        })
        resolve("passed")
    })
}

connection.connect(function(err) {
    if(err) throw err
})

const mariadbCreateEntry = async (createdOn, sensorTemp, sensorHumidity, consumedAirtime, RSSI, SNR, channelRSSI, gatewayID, gatewayEUI, deviceID, deviceAddr, frequency, spreadingFactor, message) => {

    return new Promise((resolve, reject) => {
        connection.execute(
            "insert into " + SQL_TABLE + " (createdOn, sensorTemp, sensorHumidity, consumedAirtime, RSSI, SNR, channelRSSI, gatewayID, gatewayEUI, deviceID, deviceAddr, frequency, spreadingFactor, message) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, JSON_COMPACT(?))",
            [createdOn, sensorTemp, sensorHumidity, consumedAirtime, RSSI, SNR, channelRSSI, gatewayID, gatewayEUI, deviceID, deviceAddr, frequency, spreadingFactor, '{}'],
            function (err, results, fields) {
                console.log(err);
                
                console.log(results) // results contains rows returned by server
                console.log(fields) // fields contains extra meta data about results, if available
            }
        )
        resolve('passed')
        console.log('Passed');
    })
}

const mariadbCreateDB = async () => {
    return new Promise((resolve, reject) => {
        connection.execute(
            "CREATE OR REPLACE TABLE " + SQL_TABLE + " (id int(11) NOT NULL AUTO_INCREMENT," +
            "createdOn datetime," +
            "sensorTemp varchar(30)," +
            "sensorHumidity varchar(30)," +
            "consumedAirtime double," +
            "RSSI varchar(30)," +
            "SNR varchar(30)," +
            "channelRSSI varchar(30)," +
            "gatewayID varchar(30)," +
            "gatewayEUI varchar(30)," +
            "deviceID varchar(30)," +
            "deviceAddr varchar(30)," +
            "frequency varchar(30)," +
            "spreadingFactor varchar(30)," +
            "message mediumtext," +
            "message KEY (id)" +
            ");"
        )
        resolve("pased")
    })
}

const mariadbDropDB = async () => {
    return new Promise((resolve, reject) => {
        connection.execute(
            "DROP TABLE IF EXISTS " + SQL_TABLE + ";",
            function(err, results, fields) {
                if(err) return reject()
                resolve()
            }
        )
    })
}

const mariadbClearDB = async () => {
    return new Promise((resolve, reject) => {
        connection.execute(
            "TRUNCATE TABLE ttn." + SQL_TABLE + ";",
            function(err, results, fields) {
                if(err) return reject()
                resolve()
            }
        )
    })
}

export default mariadbCreateEntry