import * as mqtt from "mqtt" // import everything inside the mqtt module and give it the namespace "mqtt"

let options = {
    clientId: 'SimonBoonstra',
    port: 5000,
    keepalive: 60,
    username: 'BSI',
    protocolId: 'MQTT',
    protocolVersion: 5,
    clean: true,
    userProperties: 'test'
}

let client = mqtt.connect("mqtt://192.168.1.20", options)

client.on('connect', function() {
    client.subscribe('bsi', function(err) {
        if(!err) {
            client.publish('bsi', 'Hello mqtt')
        }
    })
})

client.on('message', function(topic, message) {
    // message is Buffer
    console.log(message.toString())
    client.end()
})

client.on('reconnect', function() {
    console.log('MeineFunktion')

})

client.addListener('message', function() {
    console.log('Superfunktion')
})

// client.reconnect()