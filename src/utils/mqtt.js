import mariadbCreateEntry from "./mariaDB.js";

// Nachrichten formatieren und MySQL db Eintrag aufrufen
function mqtt_messsageReceived(message) {
    let sensorTemp = "";
    let sensorHumidity = "";
    let consumedAirtime = "";
    let RSSI;
    let SNR;
    let channelRSSI;
    let gatewayID;
    let gatewayEUI;
    let deviceID;
    let deviceAddr;
    let frequency;
    let spreadingFactor;

    let createdOn = message.received_at.replace("T", " ").replace("Z", "").substring(0, 26);
    console.log(createdOn)

    try {
        sensorTemp = message.uplink_message.decoded_payload.temperature;
    }
    catch {
        sensorTemp = "";
        console.log("Temperatur fehlt");
    }

    try {
        sensorHumidity = message.uplink_message.decoded_payload.humidity;
    }
    catch {
        sensorHumidity = "";
        console.log("Humidity fehlt");
    }

    try {
        consumedAirtime = message.uplink_message.consumed_airtime.replace("s", "");
    }
    catch {
        consumedAirtime = "";
    }

    try {
        RSSI = message.uplink_message.rx_metadata[0].rssi;
        SNR = message.uplink_message.rx_metadata[0].snr;
        channelRSSI = message.uplink_message.rx_metadata[0].channel_rssi;
        gatewayID = message.uplink_message.rx_metadata[0].gateway_ids.gateway_id;
        gatewayEUI = message.uplink_message.rx_metadata[0].gateway_ids.eui;
    }
    catch {
        RSSI = "";
        SNR = "";
        channelRSSI = "";
        gatewayID = "";
        gatewayEUI = "";
        console.log("No rx_metadata");
    }

    deviceID = message.end_device_ids.device_id;
    deviceAddr = message.end_device_ids.dev_addr;

    try {
        frequency = message.uplink_message.settings.frequency;
        spreadingFactor = message.uplink_message.settings.data_rate.lora.spreading_factor;
    }
    catch (error) {
        frequency = "";
        spreadingFactor = "";
        console.log('No "Settings"');
    }

    mariadbCreateEntry(
        createdOn,
        sensorTemp,
        sensorHumidity,
        consumedAirtime,
        RSSI,
        SNR,
        channelRSSI,
        gatewayID,
        gatewayEUI,
        deviceID,
        deviceAddr,
        frequency,
        spreadingFactor,
        message
    );
}

export default mqtt_messsageReceived