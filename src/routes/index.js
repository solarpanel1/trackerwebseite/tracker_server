import express, { json } from 'express'
import db from '../utils/mariaDB_connect.js'
import Pagination from '../utils/pagination.js'
import { createRequire } from "module";
const require = createRequire(
    import.meta.url);
const serverInfo = require("../resources/applicationInfo.json");
const router = express.Router();
import cors from 'cors'
import auth from '../middleware/auth.js'

router.get('/status', (req, res) => {
    res.json({
        serverInfo
    })
});

router.post('/authTest', auth, async (req, res) => {
    res.send('Antwort nach erfolgreicher Authentifizierung')
});

router.post('/ttnAPI', auth, cors(), async(req, res, next) => {
    // CHAPTER Hauptteil
    let page_id = parseInt(req.query.page)
    let currentPage = page_id > 0 ? page_id : currentPage
    let pageUri = '/items/'
    let sql = 'SELECT COUNT(id) as totalCount FROM tbl_ttn'
    let totalCount = ""

    // CHAPTER Datenbank Abfrage
    db.execute(sql, (err, rows) => {
        if(err) throw err
        totalCount = rows[0].totalCount

        // Display x items per page
        let perPage = req.query.limit
        const Paginate = new Pagination(totalCount, currentPage, pageUri, perPage)
        const totalPages = Math.ceil(totalCount / perPage)

        // Query DB
        sql = 'SELECT * FROM tbl_ttn ORDER BY createdON DESC LIMIT ' + Paginate.perPage + ' OFFSET ' + (req.query.page - 1) * Paginate.perPage
        db.execute(sql, (err, data) => {
            if(err) throw err
            res.json(data)

        })
        res.status(200).send
    })
})

router.post('/totalPages', auth, cors(), (req , res) => {
    let sql = 'SELECT COUNT(id) as totalCount FROM tbl_ttn'
    db.execute(sql, (err, rows) => {
        res.json(rows)
    })
})

export default router;