import https from 'https'
import fs from 'fs'
import express from 'express';
import indexRouter from './routes/index.js'
import cors from 'cors'
import pm2apps from '../config/ecosystem.config.cjs'
import ip from 'ip'
import * as mqtt from "mqtt";
import mqtt_messsageReceived from './utils/mqtt.js'
import bodyParser from 'body-parser'

var privateKey = fs.readFileSync('./config/ssl_cert/key.pem', 'utf8');
var certificate = fs.readFileSync('./config/ssl_cert/cert.pem', 'utf8');

var credentials = {
    key: privateKey,
    cert: certificate
};

let PORT = null
let TTN_CLIENTID
let TTN_PORT
let TTN_USERNAME
let TTN_TOPIC
let TTN_TOKEN
let TTN_BROKER_URL
let Umgebung = process.env.NODE_ENV
var options

function umgebungInitialisieren() {
    Umgebung = process.env.NODE_ENV
    if(Umgebung === 'prod') {
        PORT = pm2apps.apps[0].env_production.PORT
        TTN_CLIENTID = pm2apps.apps[0].env_production.TTN_CLIENTID
        TTN_PORT = pm2apps.apps[0].env_production.TTN_PORT
        TTN_USERNAME = pm2apps.apps[0].env_production.TTN_USERNAME
        TTN_TOPIC = pm2apps.apps[0].env_production.TTN_TOPIC
        TTN_TOKEN = pm2apps.apps[0].env_production.TTN_TOKEN
        TTN_BROKER_URL = pm2apps.apps[0].env_production.TTN_TOKEN

        options = {
            clientId: TTN_CLIENTID,
            port: TTN_PORT,
            keepalive: 60,
            username: TTN_USERNAME,
            topic: TTN_TOPIC,
            password: TTN_TOKEN,
        }
    }
    else if(Umgebung === 'dev') {
        PORT = pm2apps.apps[0].env.PORT
    }
}

umgebungInitialisieren()


const router = express.Router()

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(router)
app.use(express.json())
app.use(cors())
app.use('/', indexRouter)

// NOTE mit HTTPS
let serverIP = ip.address()
https.createServer(credentials, app).listen(PORT, () =>
    console.log(`API Server is listening on port ${PORT}\nhttps://${serverIP}:${PORT}\nhttps://${serverIP}:${PORT}/ttnAPI?page=1&limit=30\nhttp://${serverIP}:${PORT}/status`),
);

// NOTE MQTT Client
const client = mqtt.connect("mqtt://eu1.cloud.thethings.network", options)

client.on('connect', () => {
    client.subscribe('#', function(err) {})
})

client.on('message', function(topic, message, packet) {
    const NachrichtParsed = JSON.parse(String(message));
    mqtt_messsageReceived(NachrichtParsed)
    // client.end()
})

export {}