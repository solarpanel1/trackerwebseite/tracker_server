import jwt from 'jsonwebtoken'
import pm2apps from '../../config/ecosystem.config.cjs'


function ensureToken(req, res, next) {
    const bearerHeader = req.headers["authorization"];

    if(typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;

        if (req.token == pm2apps.apps[0].env_production.TOKEN_SECRET) {
            next();
        } else {
            res.sendStatus(403)
        }
    }
    else {
        res.sendStatus(403);
    }
}

export default ensureToken