const pm2apps = {
    apps: [{
        script: './src/index.js',
        watch: '.',
        env: {
            name: 'SlrTrkAPI_DEV',
            PORT: 2000,
            watch: '.',
            NODE_ENV: 'dev',
            SQL_HOST: '192.168.x.x',
            SQL_USER: 'ttn',
            SQL_PASSWORD: 'your SQL Password',
            SQL_DB: 'ttn',
            SQL_TABLE: 'tbl_ttn_testing',
            TTN_TOPIC: '#',
            TTN_CLIENTID: 'MyMQTT',
            TTN_PORT: '1883',
            TTN_BROKER_URL: 'mqtt://eu1.cloud.thethings.network',
            TTN_USERNAME: 'projectName@ttn',
            TTN_TOKEN: 'NNSXS.xxxxxxxxxxxxxxxxxxxxxxxxxxxx.xxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        },
        env_production: {
            name: 'SlrTrkAPI_PROD',
            PORT: 3000,
            watch: '.',
            NODE_ENV: 'prod',
            SQL_HOST: '192.168.x.x',
            SQL_USER: 'ttn',
            SQL_PASSWORD: 'your SQL Password',
            SQL_DB: 'ttn',
            SQL_TABLE: 'tbl_ttn',
            TTN_TOPIC: '#',
            TTN_CLIENTID: 'MyMQTT',
            TTN_PORT: '1883',
            TTN_BROKER_URL: 'mqtt://eu1.cloud.thethings.network',
            TTN_USERNAME: 'projectName@ttn',
            TTN_TOKEN: 'NNSXS.xxxxxxxxxxxxxxxxxxxxxxxxxxxx.xxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        }
    }]
}

module.exports = pm2apps